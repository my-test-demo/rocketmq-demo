package com.example.demo.consumer;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.springframework.beans.factory.annotation.Value;

import java.util.UUID;

/**
 * @ClassName PushConsumer
 * @Description TODO
 * @Author lyk
 * @Date 2019/3/12 13:52
 * @Version 1.0
 **/
public class PushConsumer {

    public static void main(String[] args) throws MQClientException, InterruptedException {
        //生成Consumer
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("group_1");
        //配置Consumer
//        consumer.setInstanceName(UUID.randomUUID().toString());
        consumer.setMessageModel(MessageModel.BROADCASTING);
        consumer.setConsumeMessageBatchMaxSize(32);
        consumer.setNamesrvAddr("47.102.110.97:9876");
//        consumer.setNamesrvAddr("192.168.56.102:9876");
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);
        consumer.registerMessageListener(
                (MessageListenerConcurrently)(list, consumeConcurrentlyContext) -> {
                    //消费消息
                    for(MessageExt me : list) {
                        System.out.println("msgId:"+me.getMsgId()+"；msg:" + new String(me.getBody()));
                    }
                    return ConsumeConcurrentlyStatus.CONSUME_SUCCESS; });
        //启动Consumer
        consumer.subscribe("CONSUMER-TEST", "*");
        consumer.start();
        System.out.println("Consumer start......");
        //停止Consumer
        Thread.sleep(6000);
        consumer.shutdown();
    }
}
