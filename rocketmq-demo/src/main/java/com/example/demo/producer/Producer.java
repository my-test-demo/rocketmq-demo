package com.example.demo.producer;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.util.UUID;

/**
 * @ClassName Producer
 * @Description TODO
 * @Author lyk
 * @Date 2019/3/12 9:45
 * @Version 1.0
 **/
public class Producer {
    public static void main(String[] args) {

        //创建Producer
        DefaultMQProducer producer = new DefaultMQProducer("pro_test");
        //配置Producer
//        producer.setNamesrvAddr("192.168.56.102:9876");
        producer.setNamesrvAddr("47.102.110.97:9876");
        producer.setVipChannelEnabled(false);
//        producer.setInstanceName(UUID.randomUUID().toString());
        //启动Producer
        try {
            producer.start();
            System.out.println("Producer start......");
        } catch (MQClientException e) {
            e.printStackTrace();
        }
        //生产消息
        String str = "Hello RocketMQ! this is a test message.";
        Message msg = new Message("CONSUMER-TEST",UUID.randomUUID().toString(), str.getBytes());
        try {
            producer.send(msg);
        } catch (MQClientException | RemotingException | MQBrokerException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            //停止Producer
            producer.shutdown();
            System.out.println("Producer shutdown......");
        }


    }

    public void sentMQMessage() {
        DefaultMQProducer producer = new DefaultMQProducer("Producer");
        producer.setNamesrvAddr("47.102.110.97:9876");
        producer.setInstanceName("Producer-instance");
        try {
            producer.start();
            String message = "{\"airForbidden\":\"0\",\"auxOpCode\":\"NEW\",\"businessTypeCode\":\"EXP\",\"createOrgCode\":\"210501\",\"createTerminal\":\"\",\"createTime\":\"2019-07-12 13:35:02\",\"createUserCode\":\"01581176\",\"createUserName\":\"阴永刚\",\"delegateOrgCode\":\"210991\",\"desCity\":\"上海市\",\"desCountry\":\"310112\",\"desOrgCode\":\"210505\",\"desProv\":\"310000\",\"deviceType\":\"walk\",\"effectiveTypeCode\":\"7010\",\"empCode\":\"01581176\",\"empName\":\"阴永刚\",\"expType\":\"10\",\"expressContentCode\":\"G9\",\"expressContentName\":\"其他类\",\"feeAmt\":0.02,\"feeFlag\":\"1\",\"goods\":\"其他\",\"id\":\"f539eac3-35b5-42f2-bc85-058464bfdf02\",\"ieFlag\":\"\",\"inputWeight\":0.01,\"isOnline\":\"1\",\"modifyOrgCode\":\"\",\"modifyTerminal\":\"\",\"modifyTime\":\"2019-07-12 13:35:02\",\"modifyUserCode\":\"\",\"modifyUserName\":\"\",\"opCode\":311,\"orderNo\":\"OP789275698102272\",\"orgCode\":\"210501\",\"orgType\":\"1050\",\"payType\":\"\",\"pkgHeight\":0,\"pkgLength\":0,\"pkgQty\":0,\"pkgWidth\":0,\"refId\":\"\",\"remark\":\"\",\"returnWaybillNo\":\"\",\"snNo\":\"\",\"sourceOrgCode\":\"210501\",\"status\":\"1\",\"transferStatus\":\"3\",\"transportTypeCode\":\"\",\"uploadTime\":\"2019-07-12 13:35:17.971\",\"volumeWeight\":0.00,\"waybillNo\":\"B90008168665\",\"weighWeight\":0}";
            Message msg = new Message("OPMS-OP-RECEIVE-TAKING",
                    "311", UUID.randomUUID().toString(),
                    message.getBytes("UTF-8"));
            producer.setVipChannelEnabled(false);
            producer.sendOneway(msg);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            producer.shutdown();
        }
    }


}

